'use strict';

let routes = require('./routes');

let express = require('express');
let path = require('path');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let cors = require('cors');
mongoose.connect('mongodb://@34.74.172.189:27017/vendimia');
let app = module.exports = express();


app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(routes.activeSells);
app.use(routes.article);
app.use(routes.client);
app.use(routes.config);
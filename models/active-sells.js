'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let activeSells = new Schema({
  folioVenta : String,
  claveCliente : String,
  nombre : String,
  total : Number,
  fecha: String
});

module.exports = mongoose.model('ActiveSells', activeSells);

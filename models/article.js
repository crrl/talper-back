'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let article = new Schema({
  claveArticulo : String,
  descripcion : String,
  modelo : String,
  precio : Number,
  existencia : Number
});

module.exports = mongoose.model('Article', article);

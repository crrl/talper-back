'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let client = new Schema({
  claveCte : String,
  nombre : String,
  apPaterno : String,
  apMaterno : String,
  RFC : String
});

module.exports = mongoose.model('Client', client);

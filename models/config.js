'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let configuration = new Schema({
  clave: String,
  tasa : Number,
  porcEnganche : Number,
  plazoMaximo : Number
});

module.exports = mongoose.model('Configuration', configuration);

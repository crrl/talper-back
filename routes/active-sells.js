'use strict';
let express = require('express');
let router = express.Router();
let app = require('../app');

let activeSells = require('../models/active-sells');
let article = require('../models/article');

router.post('/api/activeSells', (req, res) => {
  if (!req.body.folioVenta || !req.body.claveCliente ||
      !req.body.nombre || !req.body.total) {
    res.status(400).send({'error': 'Faltan datos.'});
    return;
  }

  req.body.articulos.forEach(function (articulo) {
    articulo.existencia = articulo.existencia - articulo.cantidad;
    article.update({'claveArticulo': articulo.claveArticulo}, articulo, (err, values) => {
      if (!err) {
      } else {
        res.write("Ha ocurrido un error.");
      }
    });
  });


  let newSell = new activeSells({
    folioVenta : req.body.folioVenta,
    claveCliente : req.body.claveCliente,
    nombre: req.body.nombre,
    total: req.body.total,
    fecha: req.body.fecha
  });

  newSell.save((err) => {
    if (err) {
      return err;
    }
  })
  .then( () => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Se ha agregado con éxito.');
  });
});



router.get('/api/activeSells', function(req, res) {
 activeSells.find(function(err, result) {
   if (err) {
   } else {
      res.send(result);
   }
  });
});


router.get('/api/activeSells/:id', function(req, res) {
 activeSells.find({'_id':req.params.id})
 .then(function (response) {
   res.send(response);
 })
 .catch(function() {
    res.writeHead(400, {'Content-Type':'text/plain'});
    res.end('No hay usuarios con esa clave.');
  });
});


router.delete('/api/activeSells/:id', (req, res) => {
  activeSells.remove({'id': req.params.id},function(err) {
    if (!err) {
        res.json("Se ha eliminado correctamente.");
    } else {
        res.write("Ha ocurrido un error.");
    }
  });
});


module.exports = router;
'use strict';
let express = require('express');
let router = express.Router();

let article = require('../models/article');

router.post('/api/article', (req, res) => {
  if (!req.body.descripcion ||
      !req.body.precio || !req.body.existencia) {
    res.status(400).send({'error': 'Faltan datos.'});
    return;
  }

  let newArticle = new article({
    claveArticulo : req.body.claveArticulo,
    descripcion : req.body.descripcion,
    modelo : req.body.modelo,
    precio: req.body.precio,
    existencia: req.body.existencia
  });

  newArticle.save((err) => {
    if (err) {
      return err;
    }
  })
  .then( () => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Se ha agregado con éxito.');
  });
});


router.get('/api/article', function(req, res) {
 article.find(function(err, result) {
   if (err) {
   } else {
      res.send(result);
   }
  });
});


router.get('/api/article/:id', function(req, res) {
 article.findOne({'claveArticulo':req.params.id})
 .then(function (response) {
   res.send(response);
 })
 .catch(function() {
    res.writeHead(400, {'Content-Type':'text/plain'});
    res.end('No hay usuarios con esa clave.');
  });
});



router.put('/api/article/:id', (req, res) => {
  article.update({'claveArticulo': req.params.id}, req.body, (err, values) => {
      if (!err) {
          res.json("Se ha actualizado correctamente.");
      } else {
          res.write("Ha ocurrido un error.");
      }
  });
});


router.delete('/api/article/:id', (req, res) => {
  article.remove({'id': req.params.id},function(err) {
    if (!err) {
        res.json("Se ha eliminado correctamente.");
    } else {
        res.write("Ha ocurrido un error.");
    }
  });
});

module.exports = router;
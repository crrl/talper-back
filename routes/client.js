'use strict';
let express = require('express');
let router = express.Router();

let client = require('../models/client');

router.post('/api/client', (req, res) => {
  if (!req.body.claveCte || !req.body.apPaterno || !req.body.nombre ||
      !req.body.apMaterno || !req.body.RFC) {
    res.status(400).send({'error': 'Faltan datos.'});
    return;
  }

  client.find({ 'claveCte': req.body.claveCte }, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      if (result.length > 0) {
        res.status(400).send({'error': 'Ya existe usuario con esa clave.'});
        return;
      } else {
        let newClient = new client({
          claveCte : req.body.claveCte,
          nombre : req.body.nombre,
          apPaterno : req.body.apPaterno,
          apMaterno: req.body.apMaterno,
          RFC: req.body.RFC
        });

        newClient.save((err) => {
          if (err) {
            return err;
          }
        })
        .then( () => {
          res.writeHead(200, {'Content-Type': 'text/plain'});
          res.end('Se ha agregado con éxito.');
        });
      }
    }
  });
});


router.get('/api/client', function(req, res) {
 client.find(function(err, result) {
   if (err) {
   } else {
      res.send(result);
   }
  });
});


router.get('/api/client/:claveCte', function(req, res) {
 client.findOne({'claveCte':req.params.claveCte})
 .then(function (response) {
   res.send(response);
 })
 .catch(function() {
    res.writeHead(400, {'Content-Type':'text/plain'});
    res.end('No hay usuarios con esa clave.');
  });
});



router.put('/api/client/:claveCte', (req, res) => {
  client.update({'claveCte': req.params.claveCte}, req.body, (err, values) => {
      if (!err) {
          res.json("Se ha actualizado correctamente.");
      } else {
          res.write("Ha ocurrido un error.");
      }
  });
});

router.delete('/api/client/:claveCte', (req, res) => {
  client.remove({'claveCte': req.params.claveCte},function(err) {
    if (!err) {
        res.json("Se ha eliminado correctamente.");
    } else {
        res.write("Ha ocurrido un error.");
    }
  });
});

module.exports = router;
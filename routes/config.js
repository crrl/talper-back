'use strict';
let express = require('express');
let router = express.Router();

let config = require('../models/config');

router.post('/api/config', (req, res) => {
  config.find((err, result) => {
    if (err) {
      return err;
    } else {
      if (result.length > 0) {
        config.update({'clave': '0001'}, req.body, (err, values) => {
            if (!err) {
                res.json("Se ha actualizado correctamente.");
            } else {
                res.write("Ha ocurrido un error.");
            }
        });
      } else {
        let newConfig = new config({
          clave : '0001',
          tasa : req.body.tasa,
          porcEnganche : req.body.porcEnganche,
          plazoMaximo: req.body.plazoMaximo,
        });

        newConfig.save((err) => {
          if (err) {
            return err;
          }
        })
        .then( () => {
          res.writeHead(200, {'Content-Type': 'text/plain'});
          res.end('Se ha agregado con éxito.');
        });
      }
    }
  });
});



router.get('/api/config', function(req, res) {
 config.find(function(err, result) {
   if (err) {
   } else {
      res.send(result);
   }
  });
});

module.exports = router;